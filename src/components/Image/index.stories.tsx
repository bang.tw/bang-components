import React from 'react';
import { ComponentStory, ComponentMeta } from '@storybook/react';

import Main from '.';

export default {
  title: 'Components/Image',
  component: Main,
} as ComponentMeta<typeof Main>;

const Template: ComponentStory<typeof Main> = (args) => <Main {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  src: 'https://fakeimg.pl/250x100/',
  width: 250,
  height: 250,
};
