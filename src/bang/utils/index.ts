import { eArray } from './e-array';
import { eDateTime } from './e-datetime';
import eMap from './e-map';
import { eNumber } from './e-number';
import { ePagination } from './e-pagination';
import { eString } from './e-string';
import Validators, { ObjToArray } from './validators';
import ValidatorsFeedback from './validators/feedback';
import { InputValidator } from './validators/input-validator';

const Email = Validators.Email;
const MobilePhoneNumber = Validators.MobilePhoneNumber;
const Password = Validators.Password;
const EmailOrMobileNumber = Validators.EmailOrMobileNumber;
const Required = Validators.Required;
const NumbersOnly = Validators.NumbersOnly;
const Length = Validators.Length;
const Equals = Validators.Equals;
const DateFormat = Validators.DateFormat;
const InDataList = Validators.InDataList;

export {
  eMap,
  eArray,
  eDateTime,
  eNumber,
  ePagination,
  eString,
  ValidatorsFeedback,
  Email,
  MobilePhoneNumber,
  Password,
  EmailOrMobileNumber,
  Required,
  NumbersOnly,
  InputValidator,
  Length,
  InDataList,
  Equals,
  DateFormat,
  ObjToArray,
  Validators,
};
