export interface ePaginationProps {
  total?: number;
  currentPage?: number;
  itemsPerPage?: number;
}

export interface ePaginationResult {
  total: number;
  totalPage: number;
  currentPage: number;
  itemsPerPage: number;
}

export class ePagination {
  constructor(props?: ePaginationProps) {
    this.total = props?.total ?? 9999999;
    this.currentPage = props?.currentPage ?? 1;
    this.itemsPerPage = props?.itemsPerPage ?? 10;
  }
  private total: number;
  private currentPage: number;
  private itemsPerPage: number;

  public getResult(): ePaginationResult {
    return {
      currentPage: this.getCurrentPage(),
      total: this.getTotal(),
      totalPage: this.getTotalPages(),
      itemsPerPage: this.getItemsPerPage(),
    };
  }

  public getTotal() {
    return this.total;
  }

  public getCurrentPage() {
    return this.currentPage;
  }

  public getItemsPerPage() {
    return this.itemsPerPage;
  }

  public getStartPage() {
    const result1 = Math.floor((this.currentPage - 1) / 10.0);
    const result = result1 * 10 + 1;
    return result;
  }

  public getStartToEndPages() {
    const startPage = this.getStartPage();
    const endPage = this.getEndPage();
    let result = [];
    for (let index = startPage; index <= endPage; index++) {
      result.push(index);
    }
    return result;
  }

  public getEndPage() {
    const result = this.getStartPage() + 10 - 1;
    const totalPages = this.getTotalPages();
    if (result > totalPages) {
      return totalPages;
    }
    return result;
  }
  public getTotalPages(newTotal?: number) {
    if (newTotal) {
      this.total = newTotal;
    }
    const result1 = Math.ceil(this.total / parseFloat(this.itemsPerPage + ''));
    const pages = parseInt(result1 + '');
    return pages < 1 ? 1 : pages;
  }
  public getPrevTenPage() {
    const startPage = this.getStartPage();
    const result = startPage - 10 < 1 ? 1 : startPage - 10;
    return result;
  }

  public hasPrevTenPages() {
    return this.getStartPage() > 10;
  }

  public getPrevPage() {
    return this.currentPage - 1 < 1 ? 1 : this.currentPage - 1;
  }

  public hasPrevPage() {
    return this.currentPage > 1;
  }
  public hasNextTenPages() {
    return this.getTotalPages() > this.getEndPage();
  }
  public getNextTenPage() {
    const endPage = this.getEndPage();
    const totalPages = this.getTotalPages();
    return endPage + 1 > totalPages ? totalPages : endPage + 1;
  }

  public hasNextPage() {
    return this.getTotalPages() > this.currentPage;
  }

  public getNextPage() {
    const totalPage = this.getTotalPages();
    const result1 = this.currentPage + 1 > totalPage;
    return result1 ? totalPage : this.currentPage + 1;
  }

  public getSkipItems() {
    let totalPages = this.getTotalPages();
    if (this.currentPage > totalPages) {
      this.currentPage = totalPages;
    }
    return this.itemsPerPage * (this.currentPage - 1);
  }
  public getStartIndex() {
    const result = this.total === 0 ? 0 : this.getSkipItems() + 1;
    return result;
  }

  public getEndIndex() {
    const total = this.total;
    let result = this.getStartIndex() + this.itemsPerPage - 1;
    if (result > total) {
      result = total;
    }
    return result;
  }

  public getSkipCountAjaxPage() {
    return this.itemsPerPage * (this.currentPage - 1);
  }
  public toNextPage() {
    this.currentPage += 1;
  }
}
