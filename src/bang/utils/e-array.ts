export class eArray {
  public static findOne<T>(inArray: T[], filter: (obj: T) => boolean) {
    const results = inArray.filter(filter);
    if (results.length > 0) {
      return results[0];
    }
    return undefined;
  }
}
