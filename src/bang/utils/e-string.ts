export class eString {
  public static toIntOrUndefined(value?: string) {
    return value ? parseInt(value) : undefined;
  }
  public static toInt(value: string) {
    return parseInt(value);
  }

  public static toFloatOrUndefined(value?: string) {
    return value ? parseFloat(value) : undefined;
  }
  public static toFloat(value: string) {
    return parseFloat(value);
  }
}
