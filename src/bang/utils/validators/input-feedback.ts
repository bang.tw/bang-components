export type InputFeedback = 'error' | 'normal' | 'success';

export type FeedbackFunction = (msg: string, type?: InputFeedback) => void;

export type InputValidState = (data: {
  pass: boolean | undefined;
  errMsg: string;
}) => void;
