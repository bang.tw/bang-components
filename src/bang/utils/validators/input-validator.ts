import { FeedbackFunction } from './input-feedback';

export type InputValidator = (
  value: string,
  setError: FeedbackFunction
) => boolean;
