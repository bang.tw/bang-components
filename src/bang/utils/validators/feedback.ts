import { InputValidState } from './input-feedback';
import { InputValidator } from './input-validator';

interface FeedbackProps {
  value?: string;
  validators: InputValidator | InputValidator[];
  validSetState: InputValidState;
}
export default function ValidatorsFeedback(props: FeedbackProps) {
  const { value } = props;
  const validators: InputValidator[] = Array.isArray(props.validators)
    ? props.validators
    : [props.validators];

  let pass = true;
  validators.forEach((validator) => {
    pass =
      pass &&
      validator(value ?? '', (msg, type) => {
        const setValid = props.validSetState;
        if (type === undefined || type === 'error') {
          setValid({
            errMsg: msg,
            pass: false,
          });
        } else {
          setValid({
            errMsg: '',
            pass: type === 'success' ?? undefined,
          });
        }
      });
  });
  return pass;
}
