import validator from 'validator';
import matches from 'validator/lib/matches';
import { NumFromTo } from '../../types';
import { InputValidator } from './input-validator';

export default class Validators {
  public static IPv4orIPv6: InputValidator = (value, setMsg) => {
    if (value.length > 0) {
      const isIp = validator.isIP(value);
      if (!isIp) {
        setMsg('輸入的IP格式有誤！', 'error');
        return false;
      }
    }
    setMsg('', 'success');
    return true;
  };
  public static Username(): InputValidator[] {
    return [
      ...Validators.BetweenLength(3, 16),
      Validators.LetterAndNumberOnly(),
    ];
  }
  public static GreaterThanOneLetter() {
    return Validators.Regex(/[a-zA-Z]{1,}/, '請至少輸入一位英文字母。');
  }
  public static GreaterThanOneNumber() {
    return Validators.Regex(/[0-9]{1,}/, '請至少輸入一位數字。');
  }
  public static LetterAndNumberOnly() {
    return Validators.Regex(
      /^[a-zA-Z_0-9]{1,}$/,
      '請輸入半形，且不含特殊符號的英數字。'
    );
  }
  public static Password(betweenLen?: NumFromTo): InputValidator[] {
    if (!betweenLen) {
      betweenLen = {
        from: 6,
        to: 12,
      };
    }
    return [
      ...Validators.BetweenLength(betweenLen.from, betweenLen.to),
      Validators.LetterAndNumberOnly(),
    ];
  }
  public static NumbersOnly(): InputValidator {
    return Validators.Regex(/^[0-9]{1,}$/, '請輸入半形數字。');
  }
  public static MobilePhoneNumber(): InputValidator {
    return Validators.Regex(/^09[0-9]{8}$/, '請輸入正確的手機號碼。');
  }
  public static IdNumber(): InputValidator {
    return Validators.Regex(/^[A-Z]{1}[0-9]{9}$/, '請輸入正確的身分證字號。');
  }
  public static InDataList(
    dataList: string[],
    customError?: string
  ): InputValidator {
    const resultValidator: InputValidator = (value, setMsg) => {
      if (value.length > 0) {
        if (dataList.indexOf(value) < 0) {
          setMsg(customError ?? '請輸入符合的資料內容！', 'error');
          return false;
        }
      }
      setMsg('', 'success');
      return true;
    };
    return resultValidator;
  }
  public static Equals(
    equalsValue: string,
    customError?: string
  ): InputValidator {
    const resultValidator: InputValidator = (value, setMsg) => {
      if (value.length > 0) {
        if (value !== equalsValue) {
          setMsg(customError ?? '輸入的內容不符！', 'error');
          return false;
        }
      }
      setMsg('', 'success');
      return true;
    };
    return resultValidator;
  }
  public static Regex(regex: RegExp, customError?: string): InputValidator {
    const resultValidator: InputValidator = (value, setMsg) => {
      if (value.length > 0) {
        if (!matches(value, regex)) {
          setMsg(customError ?? '輸入的格式有誤！', 'error');
          return false;
        }
      }
      setMsg('', 'success');
      return true;
    };
    return resultValidator;
  }
  public static Length(length: number): InputValidator {
    const resultValidator: InputValidator = (value, setMsg) => {
      if (value.length > 0) {
        if (value.length !== length) {
          setMsg(`請輸入「${length}」個字元。`, 'error');
          return false;
        }
      }
      setMsg('', 'success');
      return true;
    };
    return resultValidator;
  }

  public static BetweenLength(
    min: number,
    max: number,
    customError?: string
  ): InputValidator[] {
    return [
      Validators.MinLength(min, `請輸入「${min} ~ ${max}」個字元`),
      Validators.MaxLength(max, `請輸入「${min} ~ ${max}」個字元`),
    ];
  }

  public static MinLength(
    length: number,
    customError?: string
  ): InputValidator {
    const resultValidator: InputValidator = (value, setMsg) => {
      if (value.length > 0) {
        if (value.length < length) {
          setMsg(customError ?? `請輸入「${length}」個字元以上`, 'error');
          return false;
        }
      }
      setMsg('', 'success');
      return true;
    };
    return resultValidator;
  }

  public static EndWith(endWith: string, customError?: string): InputValidator {
    const resultValidator: InputValidator = (value, setMsg) => {
      if (value.length > 0 && !value.endsWith(endWith)) {
        setMsg(customError ?? `輸入內容結尾必須為"${endWith}"`, 'error');
        return false;
      }
      setMsg('', 'success');
      return true;
    };
    return resultValidator;
  }

  public static MaxLength(
    length: number,
    customError?: string
  ): InputValidator {
    const resultValidator: InputValidator = (value, setMsg) => {
      if (value.length > 0) {
        if (value.length > length) {
          setMsg(customError ?? `請輸入「${length}」個字元以下`, 'error');
          return false;
        }
      }
      setMsg('', 'success');
      return true;
    };
    return resultValidator;
  }

  public static Required: InputValidator = (value, setMsg) => {
    if (value.length === 0) {
      setMsg('這個欄位是必填的', 'error');
      return false;
    }
    setMsg('', 'success');
    return true;
  };

  public static EmailOrMobileNumber: InputValidator = (value, setMsg) => {
    if (value.startsWith('0') && validator.isNumeric(value)) {
      const mobilePhoneNumber = Validators.MobilePhoneNumber();
      return mobilePhoneNumber(value, setMsg);
    } else {
      if (value.length > 0 && !validator.isEmail(value)) {
        setMsg('Email格式有誤！', 'error');
        return false;
      } else {
        setMsg('', 'success');
        return true;
      }
    }
  };

  public static Email: InputValidator = (value, setMsg) => {
    if (value.length > 0 && !validator.isEmail(value)) {
      setMsg('Email格式有誤！', 'error');
      return false;
    } else {
      setMsg('', 'success');
      return true;
    }
  };

  public static Numeric: InputValidator = (value, setMsg) => {
    if (value) {
      if (validator.isNumeric(value)) {
        setMsg('', 'success');
      } else {
        setMsg('輸入的數字格式有誤！', 'error');
        return false;
      }
    }
    return true;
  };

  public static DateFormat: InputValidator = (value, setMsg) => {
    if (value) {
      if (validator.isDate(value)) {
        setMsg('', 'success');
      } else {
        setMsg('輸入的日期格式有誤！', 'error');
        return false;
      }
    }
    return true;
  };

  public static NumbericWithDecimals = (decimals: number): InputValidator[] => {
    const decimalsValidator: InputValidator = (value, setMsg) => {
      if (value) {
        const splitResults = value.split('.');
        if (splitResults.length === 1) {
          setMsg('', 'success');
          return true;
        }
        if (splitResults.length > 1 && splitResults[1].length > decimals) {
          setMsg(`小數點不可超過 ${decimals} 位`, 'error');
          return false;
        }
      }
      return true;
    };
    return [Validators.Numeric, decimalsValidator];
  };
}

export function ObjToArray<T>(data?: T | T[]): T[] {
  let result: T[] = [];
  if (data !== undefined) {
    if (Array.isArray(data)) {
      result = data;
    } else {
      result = [data];
    }
  }
  return result;
}
