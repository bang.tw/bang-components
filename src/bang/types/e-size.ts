export interface eSize {
  width: number;
  height: number;
}

export interface eBoxSize {
  width?: number;
  height?: number;
  maxWidth?: number;
  minWidth?: number;

  maxHeight?: number;
  minHeight?: number;
}
