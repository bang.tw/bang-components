export default interface InputBridge<T> {
  defaultValue?: T;
  onChange?: (value: T) => void;
  errorMsg?: string;
}
