import { eDateTime } from '../utils/e-datetime';

export interface DateTimeRange {
  from: eDateTime;
  to: eDateTime;
}
