export interface eMenuItem {
  label: string;
  clickAction?: eMenuAction;
}

export type eMenuAction = (() => void) | string;
