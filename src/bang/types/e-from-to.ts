import { eDateTime } from '../utils';

export interface FromTo<T> {
  from: T;
  to: T;
}
export interface NumFromTo extends FromTo<number> {}
export interface DateTimeFromTo extends FromTo<eDateTime> {}
